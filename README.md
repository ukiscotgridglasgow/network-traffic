# network_traffic.py

This script generates network bandwidth based on SNMP calls. The 64 bit versions of the counters are used. A graphite endpoint is assumed in this case, but the code should transfer reasonably straightforwardly to another storage method.

Note that this is a barebones solution, not designed to be a full network monitoring solution.