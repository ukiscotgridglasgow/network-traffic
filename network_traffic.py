#! /usr/bin/python

import socket,netsnmp,time
from operator import sub

CARBON_SERVER = CARBONSERVER
CARBON_PORT = CARBONPORT

timelapse=10
gigabit_correction=1073741824.0/8

ifHCInOctets='.1.3.6.1.2.1.31.1.1.1.6'
ifHCOutOctets='.1.3.6.1.2.1.31.1.1.1.10'
sysUpTimeInstance='.1.3.6.1.2.1.1.3.0'

traffic={}
address={}
timestamp={}
bandwidth={}
message=''

type=TYPE
rooms=ROOMS
switches=SWITCHES
prefix=PREFIX
directions=('in','out')
timestates=('t0','t')

address[(TYPE,ROOM,SWITCH)]=IP

if __name__ == "__main__":

    for timestate in timestates:
        for room in rooms:
            for switch in switches:
                session = netsnmp.Session(DestHost=address[(prefix,room,switch)], Version=VERSION, Community=COMMUNITY)
                traffic[(type,room,switch,'in',timestate)]=session.walk(netsnmp.VarList(netsnmp.Varbind(ifHCInOctets)))
                traffic[(type,room,switch,'out',timestate)]=session.walk(netsnmp.VarList(netsnmp.Varbind(ifHCOutOctets)))
	        for direction in directions:
                    traffic[(type,room,switch,direction,timestate)]=map(int,traffic[(type,room,switch,direction,timestate)])
                timestamp[(type,room,switch,timestate)]=int(session.get(netsnmp.VarList(netsnmp.Varbind(sysUpTimeInstance)))[0])/100

        if timestate=='t0':
		time.sleep(7)

    now=int(time.time())

    for room in rooms:
        for switch in switches:
            timelapse=timestamp[(type,room,switch,'t')]-timestamp[(type,room,switch,'t0')]
            correction=timelapse*gigabit_correction

	    for direction in directions:
	        bandwidth[(type,room,switch,direction)]=map(sub,traffic[(type,room,switch,direction,'t')],traffic[(type,room,switch,direction,'t0')])
	        bandwidth[(type,room,switch,direction)]=[entry/correction for entry in bandwidth[(type,room,switch,direction)]]

                # Add for loops for each type of port to be included to allow for different metric naming

                for port in range(RANGE):
		    message += 'network.'+type+'.'+room+'.'+switch+'.PORTTYPE.'+str(port)+'.'+direction+' '+str(bandwidth[(type,room,switch,direction)][port-1])+' '+str(now)+'\n'


    sock = socket.socket()
    sock.connect((CARBON_SERVER, CARBON_PORT))
    sock.sendall(message)
    sock.close()